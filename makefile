# merge: worker.o utils.o src/merge.cpp
# 	g++ src/merge.cpp worker.o utils.o -o merge -std=c++17 -I/usr/local/include/opencv4/ -L/usr/local/lib/ -lopencv_core  -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui 

generate: build/generate.o build/utils.o build/worker.o build/multithread.o
	g++ build/generate.o build/utils.o build/worker.o build/multithread.o -o generate -std=c++17 -I/usr/local/include/opencv4/ -L/usr/local/lib/ -lpthread -lopencv_core  -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui

build/generate.o: src/generate.cpp
	g++ -c src/generate.cpp -o build/generate.o -std=c++17 -I/usr/local/include/opencv4/ 

build/utils.o: src/utils.cpp src/utils.hpp
	g++ -c src/utils.cpp -o build/utils.o -std=c++17 -I/usr/local/include/opencv4/

build/worker.o: src/worker.cpp src/worker.hpp
	g++ -c src/worker.cpp -o build/worker.o -std=c++17 -I/usr/local/include/opencv4/

build/multithread.o: src/multithread.cpp src/multithread.hpp
	g++ -c src/multithread.cpp -o build/multithread.o -std=c++17 -I/usr/local/include/opencv4/

install: 
	./scripts/install_cv.sh

clean:
	./scripts/clean_folders.sh
	rm build/*.o
	# rm merge
	rm generate



