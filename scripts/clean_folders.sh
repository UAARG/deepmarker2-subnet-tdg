#!/bin/bash

rm -rf images
rm -rf labels

mkdir images
mkdir labels

touch images/.keep
touch labels/.keep