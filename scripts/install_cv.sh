#!/bin/bash


# Install minimal prerequisites (Ubuntu 18.04 as reference)
sudo apt update && sudo apt install -y cmake g++ wget unzip || sudo pacman -S --noconfirm cmake gcc wget unzip opencv



# Download and unpack sources
if [ ! -f "opencv.zip" ]; then
    wget -O opencv.zip https://github.com/opencv/opencv/archive/master.zip
fi
unzip opencv.zip && cd opencv-master
# Create build directory
mkdir -p build && cd build
# Configure
cmake  ../
# Build
cmake --build . -j4


sudo make install

#

#cd ../.. && rm -rf opencv-master opencv.zip