## XML PARSER FOR UAARG'S DATA TRAINING GENERATOR
## Author: Shawn N. Sydia AKA Hot_Chocolate
##
## Call with 'XMLParser.py [attributes]'. 
## 
## Input format: CSV (no spaces) of the following attributes
## FILEMAME, WIDTH, HEIGHT, CHANNELS, SHAPE, COLOUR,\ 
## LETTER, LETTER_COLOUR, SIN_THETA, COS_THETA, IS_EMERGENT
## 

import sys

## Formats the output (used to hide messy code lmao)  
def format(val, attr):
    
    XMLoutput = ""
    
    NEWLINE = "\n"
    TAB = "  "
    ENDattr = "/" 
    
    XMLoutput += TAB + "<" + attr + ">" + val + "<" + ENDattr + attr + ">" + NEWLINE
    
    return XMLoutput

def parser(inString):
    output = []
    XMLoutput = ""
    
    NEWLINE = "\n"
     
    delineator = ["annotation"]
    attributes = ["filename", "width", "height", "channels", "shape", "colour", "letter",\
                  "letter_colour", "sin_theta", "cos_theta", "is_emergent"]
    
    output = inString.split(',')
    
    outfile_name = output[0].split(".")[0] + ".xml"
    
    XMLoutput += "<annotation>"  + NEWLINE
    for val, attr in zip(output, attributes):
        XMLoutput += format(val, attr)
        
    XMLoutput += "</annotation>"  + NEWLINE
    
    f = open(outfile_name, "w")
    f.write(XMLoutput)
    f.close()    
    
def main():
    # test_input = "img69.png,80,80,3,rectangle,red,K,blue,0.86,0.5,False"
    
    std_input = sys.argv[1]##.strip("[]'")
    
    parser(std_input)
    
    
main()
    
    
    