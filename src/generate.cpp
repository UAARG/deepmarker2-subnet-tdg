#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <chrono>
#include <filesystem>

#include <opencv2/opencv.hpp>
// #include <opencv2/core.hpp>
// #include <opencv2/imgcodecs.hpp>
// #include <opencv2/highgui.hpp>


#include "utils.hpp"
#include "worker.hpp"
#include "multithread.hpp"


using namespace std;
using namespace cv;
namespace fs = std::filesystem;


int main(int argc, char* argv[])
{
    
    int NUM_IMAGES = 500;
    const string BACKGROUND_PATH = "assets/backgrounds/";
    const string MARKER_PATH = "assets/markers/";


    rand();
    // rand();
    rand();
    // rand();
    // rand();
    rand();
    // rand();
    // rand();
    // rand();
    
    //testing multithreaded run
    cout << "Multithreaded Run: " << endl;

    Multithread thread_mgr(NUM_IMAGES, 12);
        
        
    auto start = chrono::high_resolution_clock::now();
    thread_mgr.run("images/");
    auto stop = chrono::high_resolution_clock::now();

    cout << "check the folder for images" << endl;

    auto time_span = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Time taken: " << time_span.count()/1000.0 << "s" << endl << endl;

    return 0;
}