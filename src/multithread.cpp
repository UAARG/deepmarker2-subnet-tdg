#include <iostream>
#include <vector>
#include <filesystem>
#include <thread>
#include <opencv2/opencv.hpp>

#include "multithread.hpp"


using namespace std;
using namespace cv;
namespace fs = std::filesystem;



/**
 * Default constructor for the Multithread class. This defines 
 * the number of training data elements to generate, as well 
 * as the number of threads to use for the process. This class 
 * is a wrapper for the Worker::run() function.
 *
 * * Arguments:
 *      num_outputs (int): The number of training images to generate.
 *      n_threads (int): The number of CPU threads used for processing.
 */
Multithread::Multithread(int num_outputs, int n_threads=8) {

    this->n_threads = n_threads;
    this->num_outputs = num_outputs;


    int proc_per_thread = (int)num_outputs/(int)n_threads;

    for (int i = 0; i < n_threads-1; i++) {
        this->workers.push_back(new Worker(i*(proc_per_thread), (i+1)*proc_per_thread));
    }
    this->workers.push_back(new Worker((n_threads-1)*(proc_per_thread), num_outputs));
}

/**
 * Default destructor of Multithread. Ensures that 
 * all Worker objects that were instantiated under 
 * a Multithread object gets destroyed.
 */
Multithread::~Multithread() {
    for (int i = 0; i < this->n_threads; i++) {      
        delete this->workers[i];
        this->workers[i] = NULL;
    }
}

/**
 * Wrapper function for the Worker::run() 
 * method. This method launches n_thread 
 * instances of Worker objects (See documentation 
 * of Multithread::Multithread() for more info on 
 * the attribute n_thread), each on its own thread. 
 * Calls the Worker::run() function of each 
 * instance in parallel.
 * 
 * * Arguments:
 *      dest_path (string): Path to the folder of 
 *      which the processed images are saved. 
 */
void Multithread::run(string dest_path) {

    cout << "allocate" << endl;
    for (int i = 0; i < this->n_threads; i++) {
        this->threads.push_back(thread(&Worker::run, this->workers[i],  dest_path));
    }

    for (int i = 0; i < this->n_threads; i++) {      
        this->threads[i].join();
    }

    
}



