#ifndef MULTITHREAD_H
#define MULTITHREAD_H

#include "worker.hpp"

#include <iostream>
#include <vector>
#include <thread>
#include <string>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;


/**
 * TODO: FINISH DOCUMENTATION
 *
 *
 */
class Multithread {

    private:
        int n_threads;
        int num_outputs;
        vector<Worker*> workers;
        vector<thread> threads;


    public:
        Multithread(int num_outputs, int n_threads);
        ~Multithread();
    
        void run(string dest_path);


};




#endif