#include <iostream>
#include <filesystem>
#include <vector>

#include "utils.hpp"
#include <utility>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

using namespace std;
namespace fs=filesystem;

/**
 * ! DEPRECATED
 */
File_index::File_index(string bg_path, string marker_path)
{
    this->load_marker_paths(marker_path);
    this->load_bg_paths(bg_path);
}


/**
 * TODO: FINISH DOCUMENTATION
 *
 */
File_index::File_index(string bg_path)
{
    this->load_bg_paths(bg_path);
}

/** 
 * ! DEPRECATED
 * Loads all marker paths into the File_index object for easy retrieval
 * Expected file structure:
 * 
 *      Relative/Path/To/Markers/
 *          circles/
 *              ...png
 *          cross/
 *              ...png
 *          hexagon/
 *              ...png
 *          semicircle/
 *              ...png
 *          square/
 *              ...png
 * Args:
 *      path (string): the path to the markers folder
 * 
*/
void File_index::load_marker_paths(string path)
{
    try 
    {
        // Loop through all class folders in the Markers folder
        for (auto &main_path:fs::directory_iterator(path))
        {
            // Tokenize path
            string full_path = main_path.path().string() + "/";
            vector<string> tokens = tokenize(full_path, "/");
            string classname = tokens[tokens.size()-1];

            // loops through a class folder
            for (auto &class_path:fs::directory_iterator(full_path))
            {
                string img_path = class_path.path().string();
                if (is_png(img_path))
                {
                    this->marker_paths[classname].push_back(img_path);
                }
            }
        }
    }   
    // If path is not a directory
    catch (const fs::filesystem_error &e)
    {
        cerr << e.what() << endl;
    }

}


/**
 * Loads all background paths into the File_index object for easy retrieval
 * Expected file structure:
 *  
 *      Relative/path/to/backgrounds/
 *          ...png
 * 
 *  Args: 
 *      path (string): relative path to the backgrounds folder
 * 
*/
void File_index::load_bg_paths(string path)
{
    for (auto &bg_path:fs::directory_iterator(path))
    {
        string bg_file = bg_path.path().string();
        if (is_png(bg_file))
            this->background_paths.push_back(bg_file);
    }
}




/**
 *
 * ! DEPRECATED
 *
 */
pair<Mat, string> File_index::rand_marker()
{
    pair<Mat, string> marker;
    auto rand_class_iter = next(begin(this->marker_paths), rand() % this->marker_paths.size());
    string label = rand_class_iter->first;

    auto rand_vec_iter = next(begin(this->marker_paths[label]), rand() % this->marker_paths[label].size());
    string marker_path = *rand_vec_iter;
    
    Mat marker_img = imread(marker_path);

    marker.first = marker_img;
    marker.second = label;

    return marker;

}



/***
 * Selects a random background image and returns it. 
 * 
 * * Returns:
 *      img (Mat): A background image
 */
Mat File_index::rand_bg() 
{
    int size = this->background_paths.size();
    int rand_idx = rand()%size;
    string selected_path = this->background_paths[rand_idx];
    Mat img = imread(selected_path);
    return img;
}


/**
 *
 * ! DEPRECATED
 *
 */
void File_index::print_marker_paths()
{
    for (auto& x:this->marker_paths)
    {
        cout << x.first << ":" << endl;
        for (int i = 0; i < x.second.size(); i++)
        {
            cout << x.second[i] << endl;
        }
        cout << endl;
    }
}

/**
 * Prints all background paths
 */
void File_index::print_bg_paths()
{
    for (auto &x:this->background_paths)
    {
        cout << x << endl;
    }
    cout << endl;
}















/**
 * Breaks a string up according to the chosen delimiter. Has the ability
 * to ignore repeated delimiters
 * Examples using "/" as delimiter:
 *      "images/folder/test" -> {"images", "folder", "test"}
 *      "images/folder///test" -> {"images", "folder", "test"}
 *      "images/folder/test/" -> {"images", "folder", "test"}
 *      "images" -> {"images"}
 *      "/" -> {}
 * * Args:
 *      s (string): input string
 *      del (string): delimiter
 * * Returns:
 *      out (vector<string>): output vector containing separated substrings
*/
vector<string> tokenize(string s, string del = " ")
{
    int start = 0;
    int end = s.find(del);
    vector<string> out = {};

    while (end != -1) {
        if (end - start > 0)
            out.push_back(s.substr(start, end - start) );
        start = end + del.size();
        end = s.find(del, start);
    }

    // pushes the last element of the string into the vector
    // iff its length is non-zero
    if (s.size() - start > 0)
    {
        out.push_back(s.substr(start, s.size() - start) );
    }
    return out;
}




/**
 * Simple check for if the file in question is a PNG.
 * Checks based solely on the file extension.
 * * Args:
 *      fname (string): file path 
 *  
 * * Returns:
 *      bool: true if file is PNG, false otherwise
*/
bool is_png(string fname)
{
    int len = fname.size();
    if (tolower(fname[len-3]) == 'p' && tolower(fname[len-2]) == 'n' && tolower(fname[len-1]) == 'g')
    {
        return true;
    }
    return false;
}













/**
 * TODO: Finish Documentation
 *
 *
 */
Writer_interface::Writer_interface(int port) {

    this->run_transfer = true;

    string cmd = "python3 xmlwriter.py --port " + to_string(port);
    w_process = new thread(system, cmd.c_str());

    sleep(1);
    if ((this->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        cerr << "Socket Creation Error" << endl;
        this->w_process->join();
        exit(-1);
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        cerr << "Invalid address. Address not supported" << endl;
        this->w_process->join();
        exit(-1);
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        cerr << "Connection Failed" << endl; 
        this->w_process->join();
        exit(-1);
    }
    transfer_process = new thread(&Writer_interface::transfer, this);

}



/**
 * TODO: Finish Documentation
 *
 *
 */
Writer_interface::~Writer_interface() {

    this->run_transfer = false;


    send(this->sock, "Q", 1, 0);
    char buffer[1024] = {0};
    read(sock, buffer, 1024);
    if (string(buffer) == "A") {
        cout << "Client Received: A" << endl;
        close(this->sock);
    }
    w_process->join();
    transfer_process->join();


    delete this->transfer_process;
    delete this->w_process;
    
}


/**
 * TODO: Finish Documentation
 *
 *
 */
bool Writer_interface::buffer_is_empty() {
    return this->buffer.empty();
}


/**
 * TODO: Finish Documentation
 *
 *
 */
void Writer_interface::push(string message) {
    unique_lock<mutex> mlock(this->mt);
    this->buffer.push(message);
    mlock.unlock();
    cond.notify_one();
}




// /**
//  * TODO: Finish Documentation
//  *
//  *
//  */
// void Writer_interface::transfer() {

//     while (this->run_transfer) {
//         cout << "Waiting..." << endl;
//         this_thread::sleep_for(chrono::milliseconds(10));
//         unique_lock<std::mutex> lock(this->mt);
//         while(this->buffer.empty()) {
//             cond.wait(lock);
//         }


//         while (!this->buffer.empty()) {
//             string message = this->buffer.front();
//             this->buffer.pop();
//             send(this->sock, message.c_str(), message.length(), 0);
//             char buffer[1024] = {0};
//             int val_received = read(sock, buffer, 1024);
//             if (string(buffer) == "A") {
//                 cout << "Client Received: A" << endl;
//             } else {
//                 cerr << "Did not Receive Acknowledgement. Message was: " << message << endl;

//             }
//             // cout << "buffer size: " << this->buffer.size() << endl;
//         }
//     }

// }

/**
 * TODO: Finish Documentation
 *
 *
 */
void Writer_interface::transfer() {
    bool failed = false;
    string message;
    char rbuffer[1024] = {0};
    int curr_attempts = 0;

    while (this->run_transfer) {
        cout << "Waiting..." << endl;
        this_thread::sleep_for(chrono::milliseconds(10));
        unique_lock<std::mutex> lock(this->mt);
        while(this->buffer.empty()) {
            cond.wait(lock);
        }

        while (!this->buffer.empty()) {

            if (!failed) {
                 message = this->buffer.front();
            }

            send(this->sock, message.c_str(), message.length(), 0);
            
            memset(rbuffer, 0, 1024);

            int val_received = read(sock, rbuffer, 1024);


            if (string(rbuffer) == "A") {
                cout << "Client Received: A" << endl;
                failed = false;
            } else if (string(rbuffer) == "F" && curr_attempts < 10) {
                failed = true; 
                cerr << "Send Failed. Retrying...  " << endl;
                curr_attempts++;

                continue;
            } else {
                cerr << "Did not Receive Acknowledgement. Message was: " << message << endl;

            }
            // cout << "buffer size: " << this->buffer.size() << endl;
            this->buffer.pop();

        }
    }

}