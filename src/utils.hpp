#ifndef UTILS_HPP
#define UTILS_HPP

#include <opencv2/opencv.hpp>
#include <iostream>
#include <unordered_map>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>
#include <fcntl.h>


using namespace std;
using namespace cv;



/**
 * TODO: Finish Documentation
 *
 *
 */
class File_index {

    private:
       
        vector<string> background_paths;
        unordered_map<string, vector<string> > marker_paths; // {classname: {...png, ...png, ...png, ...}}
        void load_marker_paths(string path);
        void load_bg_paths(string path);
        
    public:
        File_index(string bg_path, string marker_path);
        File_index(string bg_path);

        pair<Mat, string> rand_marker();
        Mat rand_bg();
        
        void print_marker_paths();
        void print_bg_paths();
        

};






bool is_png(string fname);
vector<string> tokenize(string s, string del);






/**
 * TODO: Finish Documentation
 *
 *
 */
class Writer_interface {

    private:

        int sock;
        struct sockaddr_in serv_addr;

        mutex mt;
        condition_variable cond;

        queue<string> buffer;
        bool run_transfer;
        thread* transfer_process; // pushes messages over to python script
        thread* w_process; // XMLWriter python script



    
        void transfer();



    public:

        Writer_interface(int port);
        ~Writer_interface();

        bool buffer_is_empty();
        void push(string message);
};


#endif