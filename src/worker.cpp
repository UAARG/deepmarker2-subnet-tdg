#include <iostream>
#include <vector>
#include <math.h>
#include <filesystem>

#include <opencv2/opencv.hpp>

#include "utils.hpp"
#include "worker.hpp"



using namespace std;
using namespace cv;
namespace fs = std::filesystem;

// static attributes
int Worker::count = 0;
unordered_map<string, vector<vector<int> > > Worker::colour_table;
unordered_map<string, int> Worker::alpha_to_idx;
unordered_map<int, string> Worker::idx_to_alpha;

File_index* Worker::f_index = NULL;// = new File_index("assets/backgrounds");
Writer_interface* Worker::w_interface = NULL;// = new Writer_interface(42069);


vector<string> Worker::shapes;







/** 
 * TODO: FINISH DOCUMENTATION
 *
 */
Worker::Worker(int start, int end)
{
    this->start = start;
    this->end = end;

    Worker::colour_table = {
        {"BLACK", {{0, 0, 0}, {1, 5, 3}, {4, 3, 6}, {3, 2, 6}, {2, 4, 6}, {7, 4, 1}, {3, 2, 4}, {0, 1, 0}, {1, 0, 0}, {0, 0, 1}}},
        {"BROWN", {{92, 60, 9}, {91, 56, 2}, {92, 48, 2}, {89, 54, 15}, {66, 38, 0}, {82, 66, 2}, {82, 43, 2}}},
        {"RED", {{194, 67, 39}, {232, 56, 16}, {194, 36, 0}, {255, 82, 43}, {237, 12, 57}}},
        {"ORANGE", {{184, 107, 0}, {222, 140, 24}, {235, 112, 35}, {255, 111, 0}, {191, 83, 0}}},
        {"YELLOW", {{222, 218, 16}, {240, 236, 36}, {224, 221, 16}, {222, 240, 26}, {227, 209, 9}}},
        {"GREEN", {{160, 224, 9}, {50, 143, 7}, {50, 117, 19}, {65, 133, 52}, {96, 247, 67}}},
        {"BLUE", {{47, 51, 194}, {17, 87, 217}, {28, 206, 237}, {22, 7, 237}, {32, 106, 138}}},
        {"VIOLET", {{88, 37, 161}, {154, 91, 245}, {120, 28, 148}, {124, 36, 240}, {195, 124, 252}}},
        {"GRAY", {{158, 158, 158}, {135, 131, 131}, {97, 99, 96}, {81, 81, 87}, {144, 150, 147}, {191, 191, 191}, {176, 180, 184}}},
        {"WHITE", {{255, 255, 255}, {255, 250, 250}, {250, 255, 250}, {250, 250, 250}, {247, 248, 255}, {255, 255, 247}, {245, 245, 245}}}

    };
    Worker::shapes = {
        "SQUARE",
        "RECTANGLE",
        "HEXAGON",
        "SEMICIRCLE",
        "CIRCLE",
        "CROSS",
        "TRIANGLE"
    };

    int j = 0;
    for (char c = 'A'; c <= 'Z'; c++)
    {
        string s;
        s = c;
        Worker::alpha_to_idx[s] = j;
        Worker::idx_to_alpha[j] = s;
        j++;
    }

    if (Worker::count == 0) {
        Worker::f_index = new File_index("assets/backgrounds");
        Worker::w_interface = new Writer_interface(42069);
    }

    Worker::count += 1;

}







/**
 * Constructor of the Worker class. Sets this->start 
 * attribute to the default value of 0. Otherwise, 
 * everything else is identical to Worker::Worker(int start, int end).
 */
Worker::Worker(int end) : Worker(0, end) {
    if (end <= 0 ) {
        cerr << "Worker(int end): Param end cannot be less than or equal to 0" << endl;
        exit(-1);
    }
}









/**
 * TODO: FINISH DOCUMENTATION
 *
 *
 */
Worker::~Worker() {
    Worker::count -= 1;
    cout << "Remaining Workers: " << Worker::count << endl;
    if (Worker::count <= 0) {    
        cout << "Deallocating File Index" << endl;
        delete f_index;
        cout << "Deallocating Writer Interface" << endl;
        int i = 0;
        while (!w_interface->buffer_is_empty()) {
            cout << "clearing buffer " << i << endl;
            sleep(1);
            i++;
        }

        delete w_interface;
        cout  << "Done" << endl;
    }
}








/**
 * Applies a random colour filter to the input image. 
 *
 * * Arguments:
 *      img (Mat): The input OpenCV image to be modified
 * 
 * * Returns:
 *      (Mat): The resultant image after the colour filter 
 *          has been applied.
 */
Mat Worker::apply_filter(Mat img) {
    vector<Mat> channels;
    cv::split(img, channels);   
    int n_channels = min(3, img.channels());

    // Shift each channel (except for alpha)
    // by an amount between -15 to 15
    for (int i = 0; i < n_channels; i++) {

        int shift = rand()%30-15;
        channels[i] += shift;
    }
    cv::merge(channels, img);
    return img;
}





string Worker::attrib_to_csv(unordered_map<string, string> attrib) {
    string output = attrib["filename"] + ",";
    output += attrib["width"] + "," + attrib["height"] + "," + attrib["channels"] + ",";
    output += attrib["shape"] + "," + attrib["colour"] + ",";
    output += attrib["alpha"] + "," + attrib["colour_alpha"] + ",";
    output += attrib["sin_theta"] + "," + attrib["cos_theta"] + ",";
    output += "0";

    return output;
}









/** 
 * Creates a random marker Mat object and returns the annotations 
 * related to the marker. Possible shapes include SQUARE, RECTANGLE, 
 * HEXAGON, SEMICIRCLE, CIRCLE, CROSS, TRIANGLE. The marker image 
 * also contains an upper-case letter overlaid on the shape. 
 * 
 * * Returns:
 *     pair<Mat, unordered_map<string, string> >: A pair
 *              containing the marker as well as the attributes
 *              of that marker. The annotations contain the following 
 *              attributes:
 *                  shape
 *                  colour
 *                  alpha
 *                  alpha_colour
 */
pair<Mat, unordered_map<string, string> > Worker::create_marker()
{
    Mat base = Mat::zeros(Size(80, 80),CV_8UC4);
    unordered_map<string, string> attrib;
    int num_shapes = 7; // this->shape.size()
    int rand_shape = rand()%num_shapes;
    
    // Marker colour
    pair<Scalar, string> colour_marker = this->rand_colour();
    Scalar colour_val_marker = colour_marker.first;
    string colour_name_marker = colour_marker.second;

    // rand alpha
    int num_alpha = this->idx_to_alpha.size();
    int rand_idx = rand()%num_alpha;
    string alpha = this->idx_to_alpha[rand_idx];

    // rand alpha colour
    pair<Scalar, string> colour_alpha = rand_colour();
    Scalar colour_val_alpha = colour_alpha.first;
    string colour_name_alpha = colour_alpha.second;

    // retries if colour of marker and alpha are different
    while (colour_name_alpha == colour_name_marker) {
        colour_alpha = rand_colour();
        colour_val_alpha = colour_alpha.first;
        colour_name_alpha = colour_alpha.second;
    }

    Mat transparency = Mat::zeros(base.size(), CV_8UC1);

    if (rand_shape == 0) {
        // square
        rectangle(base, Rect(10, 10, 60, 60), colour_val_marker, -2);
        attrib["shape"] = "SQUARE";

    } else if (rand_shape == 1) {
        // rectangle
        int portrait = rand()%2;
        int shift = rand()%10;
        if (portrait)
            rectangle(base, Rect(20 + shift, 10, 40 - 2 * shift, 60), colour_val_marker, -2);
        else 
            rectangle(base, Rect(10, 20 + shift, 60, 40 - 2 * shift), colour_val_marker, -2);
        attrib["shape"] = "RECTANGLE";
        
    } else if (rand_shape == 2) {
        // Hexagon
        vector<Point> points = {Point(18, 4), Point(62, 4), Point(80, 40), 
                                Point(62, 76), Point(18, 76), Point(0, 40)};
        fillPoly(base, points, colour_val_marker);
        attrib["shape"] = "HEXAGON";

    } else if (rand_shape == 3) {
        // Semicircle
        ellipse(base, Point(40, 60), Size(35, 45), 180, 0, 180, colour_val_marker, -1);
        attrib["shape"] = "SEMICIRCLE";

    } else if (rand_shape == 4) {
        // Circle
        ellipse(base, Point(40, 40), Size(35, 35), 180, 0, 360, colour_val_marker, -1);
        attrib["shape"] = "CIRCLE";


    } else if (rand_shape == 5) {
        // Cross
        int shift = rand()%10;
        rectangle(base, Rect(20 + shift, 4, 40 - 2 * shift, 72), colour_val_marker, -2);
        rectangle(base, Rect(4, 20 + shift, 72, 40 - 2 * shift), colour_val_marker, -2);
        attrib["shape"] = "CROSS";

    } else {
        // Triangle
        vector<Point> points;
        if (rand()%2) 
            points = {Point(40, 2), Point(78, 78), Point(2, 78)};
        else
            points = {Point(2, 2), Point(78, 2), Point(40, 78)};
        fillPoly(base, points, colour_val_marker);
        attrib["shape"] = "TRIANGLE";

    }

    // Draw text
    int rescale = rand()%1;
    // cout << (rescale) << endl;;
    putText(base, alpha, Point(20 - 10*rescale, 55 + 5*rescale), 1+rand()%6, 2 + rescale, colour_val_alpha, 4);


    attrib["colour"] = colour_name_marker;
    attrib["alpha"] = alpha;
    attrib["colour_alpha"] = colour_name_alpha;
    

    return {base, attrib};
}









/** 
 * Changes the contrast of an image by the value alpha. 
 * An alpha of 0 represents no change to contrast, 
 * a negative value represents reduced contrast, 
 * and a positive value represents increased contrast.
 *
 * * Arguments:
 *      img (Mat): The input OpenCV image to be modified
 *      alpha (float): The contrast modifier. Recommended to be between -1 and 1.
 *
 * * Returns:
 *      out (Mat): The resultant image after contrast has been added.
 */
Mat Worker::contrast(Mat img, float alpha)
{

	float k = pow(2, alpha);
	float mu = mean(img).val[0];
	Mat out = (k*img + mu) - k * mu;


	return out;
}






/**
 * Makes a crop of dimensions (h, w) at position (x, y) on the 
 * input image. If the crop extends off the image, i.e. x + w 
 * is greater than the width of the image, or any other scenario, 
 * the cropping area would be repositioned back onto the image.
 * 
 * * Arguments:
 *      img (Mat): Input image, must be greater than cropping dimensions
 *      x (int): x coordinate of the top left corner of the 
 *               cropping window on the base image
 *      y (int): y coordinate of the top left corner of the 
 *               cropping window on the base image
 *      w (int): width of the cropping window
 *      h (int): height of the cropping window
 *
 * * Returns:
 *      (Mat): A cropped section of the original image
 *              at position (x, y) and with dimensions (h, w)
 */

Mat Worker::crop(Mat img, int x, int y, int w, int h) {

    int img_w = img.cols;
    int img_h = img.rows;

    if (h > img_h || w > img_w) {
        cerr << "crop dimensions greater than image dimensions. Halting...";
        exit(-1);
    }


    if (x + w > img_w) { 
        x = img_w - w;
    } else if (x < 0) { 
        x = 0;
    }

    if (y + h > img_h) {
        y = img_h - h;
    } else if (y < 0) {
        y = 0;
    }

    Rect crop(x, y, w, h);

    return img(crop);

}









/**
 * Merges the top image onto the bottom image using alpha blending. 
 * This method assumes the top image is a 4-channel BGRA image 
 * (otherwise, alpha blending would be ineffective), and the 
 * bottom image can be either 4-channel or 3-channel.
 *
 * * Arguments:
 *      top (Mat): The top image. Must be a 4-channel BGRA matrix
 *              and have dimensions such that the width and height 
 *              must be equal to or less than bottom.
 *      bottom (Mat): 
 *              The bottom image. Can be either 3-channel or 4-channel. 
 *      x (int): x-coordinate of the top left corner of the top image. 
 *              Defaults to 0, though you can specify a value if you 
 *              want to offset the image.
 *      y (int): y-coordinate of the top left corner of the top image. 
 *              Defaults to 0, though you can specify a value if you 
 *              want to offset the image.
 * 
 * * Returns: 
 *      output (Mat): The merged image with top on bottom.The number 
 *              of channels is the same as the number of channels of  bottom.
 */
Mat Worker::merge(Mat top, Mat bottom, int x=0, int y=0) {

    int m_rows = top.rows;
    int m_cols = top.cols;
    int m_c = top.channels();
    int m_x = 0;
    int m_y = 0;

    int b_rows = bottom.rows;
    int b_cols = bottom.cols;
    int b_c = bottom.channels();

    if (m_rows > b_rows || m_cols > b_cols) {
        cerr << "Merge failed: Dimensions of top image must be less than or equal to the dimensions of bottom image" << endl;
        exit(-1);
    }

    // handels cropping in case marker image hangs
    // off the edge of background
    if (x + m_cols <= 0 || x >= b_cols) {
        return bottom;
    } else if (x + m_cols > b_cols) {
        m_cols = b_cols - x;
    } else if (x < 0 && x + m_cols > 0) {
        m_x = -x;
        m_cols += x;
        x = 0;
    }  
    // same thing for y
    if (y + m_rows <= 0 || y >= b_rows) {
        return bottom;
    } else if (y + m_rows > b_rows) {
        m_rows = b_rows - y;
    } else if (y < 0 && y + m_rows > 0) {
        m_y = -y;
        m_rows += y;
        y = 0;
    }


    // used to make a crop of the background image
    Rect rb(x, y, m_cols, m_rows);
    // and marker image
    Rect rm(m_x, m_y, m_cols, m_rows);


    // converting the background to 4 channel
    Mat bg_4c = Mat::zeros(bottom.size(), CV_8UC4);
    if (b_c == 4) {
        bg_4c = bottom.clone();
    } else if (b_c == 3) {
        vector<Mat> channels;
        split(bottom, channels);
        channels.push_back(Mat(bottom.size(), CV_8UC1, Scalar(255)));
        cv::merge(channels, bg_4c);
    } else {
        cerr << "Background image must be either 3 channel or 4 channel" << endl;
    }

    // makes sure that marker is 4-channel
    if (m_c != 4) {
        cerr << "Marker must be 4 channel" << endl;
        exit(-1);
    }

    // Selects a section on the background image to insert the marker
    Mat aux = bg_4c.rowRange(y, y + m_rows).colRange(x, x+m_cols);
    // Pulls that section of the background and saves it as a separate Mat.
    Mat slice = bg_4c(rb);
    // Stores the channel components of background slice
    vector<Mat> b_channels;
    // convert slice to float32 and split into channels
    Mat slice_f;
    slice.convertTo(slice_f, CV_32FC4);
    slice_f /= 255;
    split(slice_f, b_channels);


    // crop marker, convert to float32, and split into channels
    Mat top_f;
    top(rm).convertTo(top_f, CV_32FC4);
    top_f /= 255;
    vector<Mat> m_channels;
    split(top_f, m_channels);


    // extract alpha channel for alpha-blending
    Mat alpha_1 = m_channels[3].clone();
    Mat alpha_2 = b_channels[3].clone();
    
    // Alpha-blending: Blends two images using the alpha channel to 
    //              selectively mask each image
    // 
    //      a = alpha channel of the markers, 0 <= a_ij <= 1
    //      m = marker matrix, 
    //      b = background matrix
    //      f = final blended image
    // 
    // Blending formula:
    //      
    //      f = a*m + (1-a)*b
    vector<Mat> combined_channels;
    for (int i = 0; i < 3; i++) {
        combined_channels.push_back(alpha_1.mul(m_channels[i]) + (Scalar(1.0)-alpha_1).mul(b_channels[i]));
    }
    combined_channels.push_back(max(alpha_1, alpha_2));


    
    cv::merge(combined_channels, slice_f);
    slice_f *= 255;
    slice_f.convertTo(slice, CV_8UC4);
    

    slice.copyTo(aux);
    Mat output = bottom.clone();
    
    //bg_4c.convertTo(output, output.type());
    if (output.channels() == 3) {
        cvtColor(bg_4c, output, 1);
    } else {
        return bg_4c;
    }

    return output;
}





/**
 * Adds gaussian noise to each of the three channels 
 * in the image. If the image has 4 channels, only 
 * the colour channels will be altered. The final 
 * image follows the formula shown below:
 *
 *      F[:, :, :3] = A[:, :, :3] + epsilon
 *      F[:, :, 3] = A[:, :, 3]
 * 
 * Where:
 *      A: The input image
 *      epsilon: Gaussian distributed noise with 
 *              zero mean and variance from 0 to 20.
 * 
 * 
 * * Arguments:
 *      img (Mat): The input image. Can be either 
 *              3-channel or 4-channel.
 * 
 * * Returns: 
 *      (Mat): The input image with gaussian noise 
 *              added to the colour channels. Output 
 *              dimensions will be identical to input 
 *              dimensions.
 * 
 */
Mat Worker::noise(Mat img) {

    Mat noise_img = Mat::zeros(img.size(), CV_8UC1);
    
    
    vector<Mat> channels;
    split(img, channels);
    for (int i = 0; i < 3; i++) {
        cv::randn(noise_img, 0, rand()%20);
        channels[i] += noise_img;
    }
    cv::merge(channels, img);
    return img;
}



















/** 
 * Picks a random colour from colour_table and returns 
 * the colour along with its name.
 * 
 * * Returns:
 *      pair<Scalar, string>: returns a cv::Scalar containing 
 *                            the BGRA values as well as a 
 *                            string containing the name of the colour.
 *                            For example, here is a possible return value:
 *                                  {cv::Scalar(0, 0, 255), “RED”}
 *
 */
pair<Scalar, string> Worker::rand_colour()
{
    // randomly chooses a colour name
    int size_colours = this->colour_table.size();
    auto random_it = next(begin(this->colour_table), rand()%size_colours);

    string colour_name = (*random_it).first;
    vector<vector<int> > colour_vals = (*random_it).second;

    // randomly chooses a variant of that colour
    int rand_rgb_idx = rand()%colour_vals.size();
    vector<int> rgb_vec = colour_vals[rand_rgb_idx];
    Scalar out = Scalar(rgb_vec[2], rgb_vec[1], rgb_vec[0], 255);

    return {out, colour_name};
}

/**
 * Randomly changes the contrast of the input image. Invokes the Worker::contrast(img, a) function with -1 <= a <= 1.
 *
 * * Arguments:
 *      img (Mat): the input image
 * * Returns: 
 *      (Mat):  The output image with the contrast changed by Worker::contrast().
 */
Mat Worker::rand_contrast(Mat img) {
    float rand_float = (float)rand()/ (float)RAND_MAX * 2 - 1;
    return this->contrast(img, rand_float);
}

// Mat Worker::rand_translate(Mat img){

// }


pair<Mat, pair<float, float> > Worker::rand_rotation(Mat img) {

    // Assumes the positive x-axis has an angle of 0 rad.
    float rand_angle = (float)rand()/ (float)RAND_MAX * 360.0;
    Mat rotated = Mat::zeros(img.rows, img.cols, img.channels());

    Point2f pcenter(img.cols/2, img.rows/2);

    Mat rmat = getRotationMatrix2D(pcenter, rand_angle, 1.0);
    warpAffine(img, rotated, rmat, img.size());

    return {rotated, {cos(rand_angle * M_PI/180), sin(rand_angle * M_PI / 180)}};
}







/**
 * TODO: Finish Documentation
 *
 *
 *
 */
void Worker::run(string dest_path) {
    for (int i = start; i < end; i++) {

        // creating background
        Mat bg = this->f_index->rand_bg();
        bg = this->crop(bg, rand()%600, rand()%400, 80, 80);

        // creating marker
        pair<Mat, unordered_map<string, string> > labeled_marker = this->create_marker();
        Mat marker = labeled_marker.first;
        unordered_map<string, string> attribs = labeled_marker.second;

        // stripe the marker
        if (rand()%3 == 0) {
            marker = this->stripes(marker);
        }

        

        // random rotation
        pair<Mat, pair<float, float> > rotated_marker = this->rand_rotation(marker);
        marker = rotated_marker.first;
        pair<float, float> angle = rotated_marker.second;

        

        // merge
        Mat img = this->merge(marker, bg, rand()%20-10, rand()%20-10);

        //cout << img.size() << ", " << img.channels() << endl;
        
        

        // contrast
        img = this->rand_contrast(img);

        // Apply colour filter
        img = this->apply_filter(img);


        // applies gaussian noise
        if (rand()%3 == 0) {
            img = this->noise(img);
        }


        // set save path
        fs::path base_path(dest_path);
        fs::path file(to_string(i) + ".jpg");
        fs::path total_dest = base_path / file;

        
        // Set attributes
        attribs["filename"] = file.string();
        attribs["width"] = to_string(img.cols);
        attribs["height"] = to_string(img.rows);
        attribs["channels"] = to_string(img.channels());

        attribs["cos_theta"] = to_string(angle.first);
        attribs["sin_theta"] = to_string(angle.second);



        string csv_str = this->attrib_to_csv(attribs);


        //send label to writer
        w_interface->push(csv_str);


        // save
        try {
            cv::imwrite(total_dest.string(), img);
            //cout << total_dest.string() << endl;
        } catch (const cv::Exception& ex){
            cerr << ex.what() << endl;
        }


    }
}






/**
 * Erases a random number of stripes (between 1 and 10)
 * from an image. Stripes can be formed in three patterns:
 * Parallel, Perpendicular, or Random.
 * * Arguments:
 *      img (Mat): Input image
 *
 * * Returns:
 *      img (Mat): Output image with stripes removed
 * 
 */
Mat Worker::stripes(Mat img){
    // Draw between 1 and 10 stripes
    int stripe_amount = (rand() % 10) + 1; 
    
    int stripe_angle = rand() % 91; // Sets angle between 0 and 90
    int img_size;
    
    int rows = img.rows; 
    int cols = img.cols;
    
    enum pattern_t {PARALLEL = 1, PERPANDICULAR = 2, RANDOM = 3};
        
    // Determine image size in pixels
    if (rows >= cols) img_size = rows;
    else img_size = cols;
    
    int pattern = (rand() % 3) + 1; // Choose a pattern to do
    
    switch (pattern){
        case PARALLEL:
            img = Worker::stripes_helper(img, stripe_amount, stripe_angle, img_size);
            break;
            
        case PERPANDICULAR: 
            img = Worker::stripes_helper(img, stripe_amount, stripe_angle, img_size);
            stripe_angle += 90;
            img = Worker::stripes_helper(img, stripe_amount, stripe_angle, img_size);
            break;
            
        case RANDOM:
            for(int i = 1; i <= stripe_amount + 1; i++){
                stripe_angle = rand() % 91;
                img = Worker::stripes_helper(img, 1, stripe_angle, img_size);
            }
            break;
    }
    
    /** for(;;){fork();} */
    
    return img;
}



/** 
 * Function which handles actually altering the bitmap. 
 * Draws alpha-coloured line from (x_I, y_I) to (x_F, y_F) of appropriate width.
 * x-axis is horizontal, y vertical, from (0, 0)
 *   
 * Start with horizontal line, rotate accordingly, translate accordingly, draw n lines.
 * use sin(angle) to calculate the start and end pixels
 *
 * * Arguments:
 *      img (Mat): opencv image object
 *      amount (int): The number of stripes to make
 *      angle (int): angle in degrees
 *      img_size (int): largest dimension of the image
 * * Returns:
 *      img (Mat): Output image with stripes removed
 */
Mat Worker::stripes_helper(Mat img, int amount, int angle, int img_size){ 
    // Convert to radians
    angle = (int) angle * M_PI / 180; 
    
    // Erase between 10 and 20% of the pixels for the given direction. 
    // May and should change these values depending on effectiveness of NN.
    int stripe_width_px = (int) (img_size / ((rand() % 11) + 10));
                                
    // Ensure that no more than n% is erased regardless of amount of stripes
    stripe_width_px = (int) stripe_width_px / amount; 
    
    // Soft assert that the calcuated value is at least 1 pixel wide. 
    stripe_width_px = (stripe_width_px > 1) ? stripe_width_px : 1;  

    // For 2 or more stripes, draw them seperated by this amount of pixels.
    // Can be variable size seperation for multiple stripes
    int stripe_seperation_px = (int) stripe_width_px * (rand() % 10 + 1);
    
    // Define start and end points for each stripe. Stripes can be drawn anywhere
    // in the image
    int x_I = 0; 
    int y_I = 0;

    int x_F = 2 * img_size; //Cropped by cv::line()
    int y_F = 0; 
    
    // Rotate
    x_F = (int) (x_F * cos(angle)) - (y_F * sin(angle));
    y_F = (int) (x_F * sin(angle)) + (y_F * cos(angle));
    
    // Start somewhere within the first half (or possible 2nd)
    int initial_offset_px = (int) img_size / (rand() % 100 + 2);
    if (amount <= 5){
        if (rand() % 2 == 0)
            initial_offset_px *= 2;
    }
        
    // Translate 
    int shift_x = (int) abs(sin(angle) * (stripe_seperation_px + initial_offset_px));
    int shift_y = (int) abs(cos(angle) * (stripe_seperation_px + initial_offset_px));
        
    for (int i = 1; i <= amount; i++){
        x_I += shift_x; x_F += shift_x;
        y_I += shift_y; y_F += shift_y;
    
        line(img, Point(x_I, y_I), Point(x_F, y_F), (0,0,0,0), stripe_width_px);
    }
    
    return img;
}

