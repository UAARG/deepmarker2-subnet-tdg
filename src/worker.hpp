#ifndef WORKER_H
#define WORKER_H

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp> // For cv::line();

#include "utils.hpp"


using namespace std;
using namespace cv;

class Worker
{
    private:
        int start;
        int end;

        static unordered_map<string, int> alpha_to_idx;
        static unordered_map<string, vector<vector<int> > >
            colour_table;
        static unordered_map<int, string> idx_to_alpha;
        static vector<string> shapes;
        
        Mat stripes_helper(Mat img, int amount, int angle, int img_size);


        

    public:
        Worker(int start, int end);
        Worker(int end);
        ~Worker();


        static int count;
        static File_index* f_index;
        static Writer_interface* w_interface;

        Mat apply_filter(Mat img);
        string attrib_to_csv(unordered_map<string, string> attrib);
        pair<Mat, unordered_map<string, string> > create_marker();
        Mat contrast(Mat img, float alpha);
        Mat crop(Mat img, int x, int y, int w, int h);
        Mat merge(Mat top, Mat bottom, int x, int y);
        Mat noise(Mat img);
        pair<Scalar, string> rand_colour();
        pair<Mat, pair<float, float> > rand_rotation(Mat img);
        Mat rand_contrast(Mat img);
        void run(string dest_path);
        Mat stripes(Mat img);


};




#endif 
