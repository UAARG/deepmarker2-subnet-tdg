import socket
import os
import argparse

HOST = '127.0.0.1'

parser = argparse.ArgumentParser()
parser.add_argument("--port", dest="PORT", default=42069, type=int)
parser.add_argument("--dest", dest="DEST", default="labels/", type=str)
args = parser.parse_args()
PORT = args.PORT
DEST = args.DEST


print(HOST)
print(PORT)

count = -1




## XML PARSER FOR UAARG'S DATA TRAINING GENERATOR
## Author: Shawn N. Sydia AKA Hot_Chocolate
##
## Call with 'XMLParser.py [attributes]'. 
## 
## Input format: CSV (no spaces) of the following attributes
## FILEMAME, WIDTH, HEIGHT, CHANNELS, SHAPE, COLOUR,\ 
## LETTER, LETTER_COLOUR, SIN_THETA, COS_THETA, IS_EMERGENT
## 

import sys

## Formats the output (used to hide messy code lmao)  
def format(val, attr):
    
    XMLoutput = ""
    
    NEWLINE = "\n"
    TAB = "  "
    ENDattr = "/" 
    
    XMLoutput += TAB + "<" + attr + ">" + val + "<" + ENDattr + attr + ">" + NEWLINE
    
    return XMLoutput

def csv_to_xml(inString):
    output = []
    XMLoutput = ""
    
    NEWLINE = "\n"
     
    delineator = ["annotation"]
    attributes = ["filename", "width", "height", "channels", "shape", "colour", "letter",\
                  "letter_colour", "sin_theta", "cos_theta", "is_emergent"]
    
    output = inString.split(',')
    
    outfile_name = output[0].split(".")[0] + ".xml"
    
    XMLoutput += "<annotation>"  + NEWLINE
    for val, attr in zip(output, attributes):
        XMLoutput += format(val, attr)
        
    XMLoutput += "</annotation>"  + NEWLINE
    
    f = open(os.path.join(DEST, outfile_name), "w")
    f.write(XMLoutput)
    f.close()    










with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print("Connected by", addr)
        while True:
            data = conn.recv(1024)
            print("Server Received:", data)
            if (data.decode() == 'Q'):
                conn.send('A'.encode())
                break
            
            csv_to_xml(data.decode())


            conn.send('A'.encode())
            count += 1

print("number of packets received:", count)